// This is the servlet.  It will return a student or date document from the servlet to the caller
//    NOTE: There probably should be an intermediate layer between this and the front end caller

package com.stindr2;

import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.Index;
import com.google.appengine.api.search.IndexSpec;
import com.google.appengine.api.search.OperationResult;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.DeleteException;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SearchServiceFactory;
import com.google.appengine.api.search.StatusCode;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StudentServlet extends HttpServlet {
    // Below is intilization stuff
    private static final long serialVersionUID = 1L;
    
    private static final String VOID_REMOVE = "Remove failed due to a null doc ID";
    private static final String VOID_ADD = "Document not added due to empty content";
    
    // First I will create the Student DB, after that works I will start bothering with the date DB
    private static final Index STUDENTDB = SearchServiceFactory.getSearchService()
            .getIndex(IndexSpec.newBuilder().setName("student_db"));
    
    enum Action {
        ADD, REMOVE, GETBUDDY, DEFAULT;
    }
    
    private static final Logger LOG = Logger.getLogger(
        StudentServlet.class.getName());
    
    // Below are the actions the DB can take
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        User currentUser = setupUser(req);
        String outcome = null;
        switch (getAction(req)) {
         case ADD:
            outcome = add(req, currentUser);
            break;
         case REMOVE:
            outcome = remove(req);
            break;
         case GETBUDDY:
            outcome = getBuddy(req);
           // Currently on Default(Search) we will fall through, this will be corrected in the future
        }
        String searchOutcome = search(req);
        if (outcome == null) {
            outcome = searchOutcome;
        }
        req.setAttribute("outcome", outcome);
        req.getRequestDispatcher("/jsp/student.jsp").forward(req, resp);
    }
    
    // This is mostly a placeholder method till I figure out how to handle users, 
    // this will probably relate to the Student_DB
    private User setupUser(HttpServletRequest req) {
        UserService userService = UserServiceFactory.getUserService();
        User currentUser = userService.getCurrentUser();
        if(currentUser != null) {
           req.setAttribute("authAction", "sign out");
           req.setAttribute("authUrl", userService.createLoginURL(req.getRequestURI()));
        }
        else {
            currentUser = new User("nobody@example.com", "example.com");
            req.setAttribute("authAction", "sign in");
            req.setAttribute("authUrl", userService.createLoginURL(req.getRequestURI()));
        }
        req.setAttribute("nickname", currentUser.getNickname());
        return currentUser;
    }
    
    // This method is responsible for adding a student object to the DB
    private String add(HttpServletRequest req, User currentUser) {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        // This will be the picture element, once I figure out how to do that
        String institution = req.getParameter("institution");
        String emailAddress = req.getParameter("emailAddress");
        String password = req.getParameter("password");
        String studyProfileResult = req.getParameter("studyProfileResult");
        
        // This will wait until I am sure this works String[] classes = req.getParameter("classes");
        if(firstName == null || lastName == null || institution == null || emailAddress == null || password == null) {
            LOG.warning(VOID_ADD);
            return VOID_ADD;
        }
        Document.Builder docBuilder = Document.newBuilder()
                .addField(Field.newBuilder().setName("firstName").setText(firstName))
                .addField(Field.newBuilder().setName("lastName").setText(lastName))
                .addField(Field.newBuilder().setName("institution").setText(institution))
                .addField(Field.newBuilder().setName("emailAddress").setText(emailAddress))
                .addField(Field.newBuilder().setName("password").setText(password))
                .addField(Field.newBuilder().setName("studyProfileResult").setText(studyProfileResult));
        
        //This is responsible for getting all the classes
        String classesStr = req.getParameter("classes");
        if (classesStr != null) {
            StringTokenizer tokenizer = new StringTokenizer(classesStr, ",");
            while (tokenizer.hasMoreTokens()) {
                docBuilder.addField(Field.newBuilder().setName("class")
                    .setText(tokenizer.nextToken()));
            }
        }
        Document doc = docBuilder.build();
        LOG.info("Adding document: \n" + doc.toString());
        try {
            STUDENTDB.put(doc);
            return "Document added";
        }
        catch(RuntimeException e) {
            LOG.log(Level.SEVERE, "Failed to add " + doc, e);
            return "Document not added due to an error " + e.getMessage();
        }
    }
    
    // Get a specific field out a document, if it is the only field
    private String getOnlyField(Document doc, String fieldName, String defaultValue) {
        if (doc.getFieldCount(fieldName) == 1) {
            return doc.getOnlyField(fieldName).getText();
        }
        LOG.severe("Field " + fieldName + " present " + doc.getFieldCount(fieldName));
        return defaultValue;
    }
    
    // A convienence method for retrieving the classes
    private List<String> getMultField(Document doc, String fieldName, String defaultVaue) {
       Iterator<Field> classIterator = doc.getFields(fieldName).iterator();
       List<String> classes = new ArrayList<String>();
       while(classIterator.hasNext()) {
           classes.add(classIterator.next().getText());
       }
       return classes;
    }
    
    // This method is responsible for searching and returning a database item
    // I will use the email address as the querable info
    private String search(HttpServletRequest req) {
        String queryStr = req.getParameter("query");
        if(queryStr == null || queryStr.equals("")) {
            req.setAttribute("found", null);
            return "Search failed due to empty search parameter";
        }
        String limitStr = req.getParameter("limit");
        int limit = 10;
        if (limitStr != null) {
            try {
                limit = Integer.parseInt(limitStr);
            } 
            catch (NumberFormatException e) {
                LOG.severe("Failed to parse "  + limitStr);
            }
        }
        List<Document> found = new ArrayList<Document>();
        String outcome = null;
        try {
            Query query = Query.newBuilder()
                    .setOptions(QueryOptions.newBuilder()
                        .setLimit(limit).build())
                    .build(queryStr);
            LOG.info("Sending query " + query);
            Results<ScoredDocument> results = STUDENTDB.search(query);
            for (ScoredDocument scoredDoc : results) {
                String firstName = getOnlyField(scoredDoc, "firstName", "");
                String lastName = getOnlyField(scoredDoc, "lastName", "");
                String institution = getOnlyField(scoredDoc, "institution", "");
                String emailAddress = getOnlyField(scoredDoc, "emailAddress", "");
                String password = getOnlyField(scoredDoc, "password", "");
                List<String> classes = getMultField(scoredDoc, "class", "");
                String studyProfileResult = getOnlyField(scoredDoc, "studyProfileResult", "");
                
                Document.Builder derived = Document.newBuilder()
                   .setId(scoredDoc.getId())
                   .addField(Field.newBuilder().setName("firstName").setText(firstName))
                   .addField(Field.newBuilder().setName("lastName").setText(lastName))
                   .addField(Field.newBuilder().setName("institution").setText(institution))
                   .addField(Field.newBuilder().setName("emailAddress").setText(emailAddress))
                   .addField(Field.newBuilder().setName("password").setText(password))
                   .addField(Field.newBuilder().setName("studyProfileResult").setText(studyProfileResult));
                
                for(String cla : classes) {
                    derived.addField(Field.newBuilder().setName("class").setText(cla));
                }
                
                found.add(derived.build());
                for(Field test : derived.build().getFields("class")) {
                    System.out.println(test.getText());
                }
            }
        } catch (RuntimeException e) {
            LOG.log(Level.SEVERE, "Search with query " + queryStr + " failed", e);   
            outcome = "Search failed due to an error: " + e.getMessage();
        }
        req.setAttribute("found", found);
        return outcome;
    }
    
    // This method will return all users which share the same 
    // Study Buddy score under a list of documents known as buddy
    private String getBuddy(HttpServletRequest req) {
        System.out.println("At least we got here");
        String searchScore = req.getParameter("searchScore");
        if(searchScore == null) {
            searchScore = "";
        }
        
        List<Document> buddies = new ArrayList<Document>();
        String outcome = null;
        try {
            Query query = Query.newBuilder()
                    .build(searchScore);
            LOG.info("Sending query" + query);
            Results<ScoredDocument> results = STUDENTDB.search(query);
            System.out.println(results.toString());
            for (ScoredDocument scoredDoc : results) {
                String firstName = getOnlyField(scoredDoc, "firstName", "");
                String lastName = getOnlyField(scoredDoc, "lastName", "");
                String institution = getOnlyField(scoredDoc, "institution", "");
                String emailAddress = getOnlyField(scoredDoc, "emailAddress", "");
                String password = getOnlyField(scoredDoc, "password", "");
                List<String> classes = getMultField(scoredDoc, "class", "");
                String studyProfileResult = getOnlyField(scoredDoc, "studyProfileResult", "");
                
                Document.Builder derived = Document.newBuilder()
                   .setId(scoredDoc.getId())
                   .addField(Field.newBuilder().setName("firstName").setText(firstName))
                   .addField(Field.newBuilder().setName("lastName").setText(lastName))
                   .addField(Field.newBuilder().setName("institution").setText(institution))
                   .addField(Field.newBuilder().setName("emailAddress").setText(emailAddress))
                   .addField(Field.newBuilder().setName("password").setText(password))
                   .addField(Field.newBuilder().setName("studyProfileResult").setText(studyProfileResult));
                
                for(String cla : classes) {
                    derived.addField(Field.newBuilder().setName("class").setText(cla));
                }
                
                buddies.add(derived.build());
            }
        } catch (RuntimeException e) {
            LOG.log(Level.SEVERE, "GetBuddy with query " + searchScore + " failed", e);
            outcome = "GetBuddy failed with error" + e.getMessage();
        }
        req.setAttribute("buddies", buddies);
        return outcome;
    }
    
    private String remove(HttpServletRequest req) {
       String[] docIds = req.getParameterValues("docid");
       if (docIds == null) {
         LOG.warning(VOID_REMOVE);
         return VOID_REMOVE;
       }
       List<String> docIdList = Arrays.asList(docIds);
       for (String pr : docIdList) {
           System.out.println(pr);
       }
       try {
         STUDENTDB.delete(docIdList);
         return "Documents " + docIdList + " removed";
       } catch (DeleteException e) {
         List<String> failedIds = findFailedIds(docIdList, e.getResults());
         LOG.log(Level.SEVERE, "Failed to remove documents " + failedIds, e);
         return "Remove failed for " + failedIds;
       }
    }
    
    private List<String> findFailedIds(List<String> docIdList,
      List<OperationResult> results) {
      List<String> failedIds = new ArrayList<String>();
      Iterator<OperationResult> opIter = results.iterator();
      Iterator<String> idIter = docIdList.iterator();
      while (opIter.hasNext() && idIter.hasNext()) {
         OperationResult result = opIter.next();
         String docId = idIter.next();
         if (!StatusCode.OK.equals(result.getCode())) {
           failedIds.add(docId);
         }
      }
      return failedIds;
    }
    
    // Gets the type of action out of the request
    private Action getAction(HttpServletRequest req) {
        if (req.getParameter("index") != null) {
            return Action.ADD;
        }
        if (req.getParameter("delete") != null) {
            return Action.REMOVE;
        }
        if (req.getParameter("searchScore") != null) {
            return Action.GETBUDDY;
        }
        return Action.DEFAULT;
    }
}