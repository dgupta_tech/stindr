function toggleSelection(cb) {
        var checked = cb.checked;
        var docCheckboxes = document.getElementsByName("docid");
        for (var i in docCheckboxes) {
          docCheckboxes[i].checked = cb.checked;
        }
      }
      function updateRangeValue(rangeId, displayId) {
        var el = document.getElementById(rangeId);
        if (el) {
          el.innerHTML = document.getElementById(displayId).value;
        }
      }