<!-- This is a test jsp to make sure the DB works correctly, and to make sure deploying works 
correctly. -->

<!DOCTYPE html>
<%@ page import="com.google.appengine.api.search.Document" %>
<%@ page import="com.google.appengine.api.search.Field" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.DateFormat" %>
<%
   String outcome = (String) request.getAttribute("outcome");
	if (outcome == null || outcome.isEmpty()) {
     outcome = "&nbsp;";
   }
	String query = (String) request.getParameter("query");
	if (query == null) {
     query = "";
   }
	String limit = (String) request.getParameter("limit");
	if (limit == null || limit.isEmpty()) {
     limit = "10";
   }
%>
<html>
  <head>
    <title>Study Buddy</title>
    <style type="text/css">
    	body{
        background-color:#FFFF99;
      }
      
      input {
        border: 1px gray solid;
        font-size: 16pt;
      }
      select {
        font-size: 16pt;
      }
      textarea {
        border: 1px gray solid;
        font-size: 16pt;
        width: 500px;
      }
      table {
        border-collapse: collapse;
      }
      td {
        border: 1px solid gray;
        padding: 5px;
      }
      th {
        border: 1px solid gray;
      }
      body {
        font-size: 14pt;
      }
    </style>
    <script type="text/javascript">
    	function toggleSelection(cb) {
        var checked = cb.checked;
        var docCheckboxes = document.getElementsByName("docid");
        for (var i in docCheckboxes) {
          docCheckboxes[i].checked = cb.checked;
        }
      }
      function updateRangeValue(rangeId, displayId) {
        var el = document.getElementById(rangeId);
        if (el) {
          el.innerHTML = document.getElementById(displayId).value;
        }
      }
    </script>
  </head>
  <body>
    <div>
      <div stype='font-size: x-large; float: left;'>
        Search for Study Buddy 
      </div>
      <div style='float: right;'>
        <%=request.getAttribute("nickname")%> &bull;
        <a href='<%=request.getAttribute("authUrl")%>'><%=request.getAttribute("authAction")%></a>
        <a href='/home.jsp'>Home</a>
      </div>
    </div>
    <div stype='clear:both; font-style: italic; margin-bottom: 1ex;'><%=outcome%></div>
	 <form name="search" action="/search" method="get">
      <input placeholder="Search" style="width:500px"
             type="search" name="query" id="query" value='<%=query%>'/>
      <select name="limit">
        <option <%="5".equals(limit)? "selected" : ""%>>5</option>
        <option <%="10".equals(limit)? "selected" : ""%>>10</option>
        <option <%="15".equals(limit)? "selected" : ""%>>15</option>
        <option <%="20".equals(limit)? "selected" : ""%>>20</option>
        <option <%="50".equals(limit)? "selected" : ""%>>50</option>
      </select>
    </form>
    <hr/>
    <form name="index" action="/search" method="get">
      <b>Manage Your Profile</b>:
      <br/>
      First Name:<textarea style="width: 500px; height: 2ex;" name="firstName"></textarea>
      <br/>
      Last Name: <textarea style="width: 500px; height: 2ex;" name="lastName"></textarea>
      <br/>
      Institution:<textarea style="width: 500px; height: 2ex;" name="institution"></textarea>
      <br/>
      Email: <textarea style="width: 500px; height: 2ex;" name="emailAddress"></textarea>
      <br/>
      Password: <textarea style="width: 500px; height: 2ex;" name="password"></textarea>
      <br/>
      Study Type:<textarea style="width: 500px; height: 2ex;" name="studyProfileResult"></textarea>
      <br/>
      Classes: <input style="width: 500px; height: 2ex;" placeholder="Comma seperated classes" name="classes" type="text" value=""/>
      <br/>
      <!--<input name="index" type="submit" value="Add" style="width: 500px;"/>-->
      <button name="index" type="submit" value="Add" style="width: 500px;">Update Profile</button>
      <input type="hidden" name="query" value="<%=query%>"/>
    </form>
    <hr/>
    <%
     List<Document> found = (List<Document>) request.getAttribute("found");
	  if (found != null) {
    %>
    <form name="delete" action="/search" method="get">
      <input type="hidden" name="query" value="<%=query%>"/>
      <table>
        <tr>
          <th>
          	<input type="checkbox" name="x" onclick="toggleSelection(this);"/>
          </th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Institution</th>
          <th>Email Address</th>
          <th>Password</th>
          <th>Study Profile Result</th>
        </tr>
      <%
      	if (found.isEmpty()) {
      %>
        	<tr>
           <td colspan='4'><i>No Matching Students</i></td>
         </tr>
      <%
    		} else {
      		for (Document doc : found) {
      %>
         <tr>
         	<td>
            	<input type="checkbox" name="docid" value="<%=doc.getId()%>"/>
            </td>
           	<td>
            	<%=doc.getOnlyField("firstName").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("lastName").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("institution").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("emailAddress").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("password").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("studyProfileResult").getText()%>
            </td>
            <%
        			for (Field classes : doc.getFields("class")) {
            %>
            <td>
           		<%=classes.getText()%>	
            </td>
            <%
      			}
            %>
         </tr>
       <%
            }
    		}
       %>
      </table>
     <!-- <input name ="delete" type="submit" value="Delete"/>-->
    </form>
    <%
     }
	 %>
<!--This is code to test the get buddy functionality it needs to be 
	 modified or deleted before submission time.-->
    <form name="getBuddy" action="/search" method="get">
      <input placeholder="Buddy Search" style="width:500px"
             type="search" name="searchScore" id="searchScore"/>
      <select name="limit">
        <option <%="5".equals(limit)? "selected" : ""%>>5</option>
        <option <%="10".equals(limit)? "selected" : ""%>>10</option>
        <option <%="15".equals(limit)? "selected" : ""%>>15</option>
        <option <%="20".equals(limit)? "selected" : ""%>>20</option>
        <option <%="50".equals(limit)? "selected" : ""%>>50</option>
      </select>
    </form>
    <%
     List<Document> buddies = (List<Document>) request.getAttribute("buddies");
	  if (buddies != null) {
    %>
    <form name="delete" action="/search" method="get">
      <input type="hidden" name="query" value="<%=query%>"/>
      <table>
        <tr>
          <th>
          	<input type="checkbox" name="x" onclick="toggleSelection(this);"/>
          </th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Institution</th>
          <th>Email Address</th>
          <th>Password</th>
          <th>Study Profile Result</th>
        </tr>
      <%
      	if (buddies.isEmpty()) {
      %>
        	<tr>
           <td colspan='4'><i>No Matching Students</i></td>
         </tr>
      <%
    		} else {
      		for (Document doc : buddies) {
      %>
         <tr>
         	<td>
            	<input type="checkbox" name="docid" value="<%=doc.getId()%>"/>
            </td>
           	<td>
            	<%=doc.getOnlyField("firstName").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("lastName").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("institution").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("emailAddress").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("password").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("studyProfileResult").getText()%>
            </td>
            <%
        			for (Field classes : doc.getFields("class")) {
            %>
            <td>
           		<%=classes.getText()%>	
            </td>
            <%
      			}
            %>
         </tr>
       <%
            }
    		}
       %>
      </table>
     <!-- <input name ="delete" type="submit" value="Delete"/>-->
    </form>
    <%
     }
	 %>
  </body>

</html>