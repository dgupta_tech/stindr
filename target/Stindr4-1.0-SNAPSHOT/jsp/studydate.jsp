<!DOCTYPE html>
<%@ page import="com.google.appengine.api.search.Document" %>
<%@ page import="com.google.appengine.api.search.Field" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.DateFormat" %>
<%
   String outcome = (String) request.getAttribute("outcome");
	if (outcome == null || outcome.isEmpty()) {
     outcome = "&nbsp;";
   }
	String query = (String) request.getParameter("query");
	if (query == null) {
     query = "";
   }
	String limit = (String) request.getParameter("limit");
	if (limit == null || limit.isEmpty()) {
     limit = "10";
   }
%>
<html>
  <head>
		<link type="text/css" rel="stylesheet" href="/css/studydate.css">
    	<!--JQuery-->
    	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    	<!--JQuery UI-->
    	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    
    	<!--Date Picker Dependencies-->
    <script src='/js/Datepair.js'></script>
    <script src='/js/jquery.datepair.js'></script>
    
    <script>
       $('#time-date .time').timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
    });

    $('#time-date .date').datepicker({
        'format': 'yyyy-m-d',
        'autoclose': true
    });

    // initialize datepair
    $('#time-date').datepair();
    </script>
  </head>
  <body>
    <div>
      <div stype='font-size: x-large; float: left;'>
        Add Study Dates
      </div>
      <div style='float: right;'>
        <%=request.getAttribute("nickname")%> &bull;
        <a href='<%=request.getAttribute("authUrl")%>'><%=request.getAttribute("authAction")%></a>
      </div>
    </div>
    <div stype='clear:both; font-style: italic; margin-bottom: 1ex;'><%=outcome%></div>
	 <form name="search" action="/studydate" method="get">
      <input placeholder="Search" style="width:500px"
             type="search" name="query" id="query" value='<%=query%>'/>
      <select name="limit">
        <option <%="5".equals(limit)? "selected" : ""%>>5</option>
        <option <%="10".equals(limit)? "selected" : ""%>>10</option>
        <option <%="15".equals(limit)? "selected" : ""%>>15</option>
        <option <%="20".equals(limit)? "selected" : ""%>>20</option>
        <option <%="50".equals(limit)? "selected" : ""%>>50</option>
      </select>
    </form>
    <hr/>
    <form name="index" action="/studydate" method="get">
      <b>Document</b>:
      <br/>
      <textarea style="width: 500px; height: 2ex;" name="starttime"></textarea>
      <br/>
      <textarea style="width: 500px; height: 2ex;" name="endtime"></textarea>
      <br/>
       <textarea style="width: 500px; height: 2ex;" name="date"></textarea>
      <br/>
      <textarea style="width: 500px; height: 2ex;" name="location"></textarea>
      <br/>
      <textarea style="width: 500px; height: 2ex;" name="subject"></textarea>
      <br/>
      <textarea style="width: 500px; height: 2ex;" name="notes"></textarea>
      <br/>
     
      <input name="index" type="submit" value="Add" style="width: 500px;"/>
      <input type="hidden" name="query" value="<%=query%>"/>
    </form>
    <hr/>
    <%
     List<Document> found = (List<Document>) request.getAttribute("found");
	  if (found != null) {
    %>
    <form name="delete" action="/studydate" method="get">
      <input type="hidden" name="query" value="<%=query%>"/>
      <table>
        <tr>
          <th>
          	<input type="checkbox" name="x" onclick="toggleSelection(this);"/>
          </th>
          <th>Start Time</th>
          <th>End Time</th>
          <th>Date</th>
          <th>Location</th>
          <th>Subject</th>
          <th>Notes</th>
         
        </tr>
      <%
      	if (found.isEmpty()) {
      %>
        	<tr>
           <td colspan='4'><i>No Matching Documents</i></td>
         </tr>
      <%
    		} else {
      		for (Document doc : found) {
      %>
         <tr>
         	<td>
            	<input type="checkbox" name="docid" value="<%=doc.getId()%>"/>
            </td>
           	<td>
            	<%=doc.getOnlyField("starttime").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("endtime").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("date").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("location").getText()%>
           	</td>
            <td>
            	<%=doc.getOnlyField("subject").getText()%>
           	</td>
            <td>
              <%=doc.getOnlyField("notes").getText()%>
            </td>
           
            <%
      			}
            %>
         </tr>
       <%
            }
    		}
       %>
      </table>
      <input name ="delete" type="submit" value="Delete"/>
    </form>
   <!-- <form name='study-date', action='/studydate', method='get'>
      <!--Date, Location, Start Time, End Time
			Subject, Comments-->
    <!--  Time and Date:
      <p id='time-date'>
      	<input type='text' class='date start' name='datestart'>
      	<input type='text' class='time start' name='timestart'>
      	<input type='text' class='time end' name='timeend'>
      	<input type='text' class='date end'name='dateend'>
      </p>
      Location: <input type='text' name='location'>
      Subject: <input type='text' name='subject'>
      Comments: <input type='text' name='comments'>
      <input type='submit' value='Submit'>
    </form> -->
  </body>
</html>
