<!DOCTYPE html>
<%@ page import="java.io.IOException" %>
<%@ page import="javax.servlet.http.HttpServlet" %>
<%@ page import="javax.servlet.http.HttpServletResponse" %>
<%@ page import="javax.servlet.jsp.JspWriter" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.google.appengine.api.users.User" %>

<html>
<head>
    <link type="text/css" rel="stylesheet" href="/css/home.css">
    <script src="home.js"></script>
  <title>Stindr-The Intellectual Hookup Site</title>
  </head>
	<body>
<%
  		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		if(user != null){
			pageContext.setAttribute("user", user);
        	pageContext.setAttribute("logoutURL", userService.createLogoutURL(request.getRequestURI()));
      }
		else{
        pageContext.setAttribute("loginURL", userService.createLoginURL(request.getRequestURI()));
      }
%>
    <div id='main-logo'>
            <img src='stindrlogo.png' alt='stindr'>
    </div>
        <nav id='main-page-nav'>
            <ul class='panel'>
              <table>
              <tr class="nav_row">
                
                <!--Login/Create Acct-->
                <td><li class='color-1'>
                    <% 
     							try{
     								if(pageContext.getAttribute("user") != null){
										out.print("<a href=" + pageContext.getAttribute("logoutURL").toString() + ">");
          
                           }
        							else{
                             	out.print("<a href=" + pageContext.getAttribute("loginURL").toString() + ">");
                           }
     								
   							}catch(Exception e){
									out.print("<a href='#'></a>");
   							}
     						%>
                        <div class='front'>
                            <i class='fa fa-home fa-4x'></i>
                        </div>
                        <div class='back'>
                          <%
                       			if(pageContext.getAttribute("user") != null){
											out.print("<span>LOGOUT</span>");
                     			}
										else{
											out.print("<span>LOGIN</span>");
                              }
                       		%>
                        </div>
                  <%out.print("</a>");%>
                  </li></td>
                <!--Post Study Date-->
                <td><li class='color-7'>
                    <% 
     								if(pageContext.getAttribute("user") != null){
                           	 out.print("<a href='/studydate'>");
                           }
									else{
										out.print("<a href=" + pageContext.getAttribute("loginURL").toString() + ">");
                           }
     						%>
                        <div class='front'>
                            <i class='fa fa-upload fa-4x'></i>
                        </div>
                        <div class='back'>
                            <span>Post Study Date</span>
                        </div>
                  <%out.print("</a>");%>
                  </li></td>
                <!--View Study Dates-->
                <td><li class='color-4'>
                    <% 
     								if(pageContext.getAttribute("user") != null){
                           	out.print("<a href='/studydate'>");     
                     		}
									else{
                             	out.print("<a href=" + pageContext.getAttribute("loginURL").toString() + ">");
                           }
     						%>
                        <div class='front'>
                            <i class='fa fa-calendar fa-4x'></i>
                        </div>
                        <div class='back'>
                            <span>View Study Date</span>
                        </div>
                  <%out.print("</a>");%>
                  </li></td>
                <!--Find Study Buddy-->
                <td><li class='color-5'>
                    <% 
     								if(pageContext.getAttribute("user") != null){
                             out.print("<a href='/search'>");   
                          	}
									else{
                             	out.print("<a href=" + pageContext.getAttribute("loginURL").toString() + ">");
                           }
     								
   							
     						%>
                        <div class='front'>
                            <i class='fa fa-group fa-4x'></i>
                        </div>
                        <div class='back'>
                            <span>Find Study Buddy</span>
                        </div>
                  <%out.print("</a>");%>
                  </li></td>
                <!--Manage Profile-->
                <td><li class='color-6'>
                    <% 
     								if(pageContext.getAttribute("user") != null){
                           	out.print("<a href='/search'>");   
                          	}
									else{
                             	out.print("<a href=" + pageContext.getAttribute("loginURL").toString() + ">");
                           }
     						
     						%>
                        <div class='front'>
                            <i class='fa fa-gears fa-4x'></i>
                        </div>
                        <div class='back'>
                            <span>Manage Profile</span>
                        </div>
                  <%out.print("</a>");%>
                  </li></td> 
                <!--About-->
                <td><li class='color-2'>
                    <a href='#'onclick='toggleAbout()'>
                        <div class='front'>
                            <i class='fa fa-user fa-4x'></i>
                        </div>
                        <div class='back'>
                            <span>ABOUT</span>
                        </div>
                    </a>
                  </li></td>
                <!--Contact-->
                <td><li class='color-3'>
                    <a href='#' onclick='toggleContact()'>
                        <div class='front'>
                            <i class='fa fa-comments fa-4x'></i>
                        </div>
                        <div class='back'>
                            <span>CONTACT</span>
                        </div>
                    </a>
                  </li></td></tr></table>
            </ul>
        </nav>
        <div id='info'>
            
        </div>
 
<!--
<a href="/studydate">To Study Date</a>
<a href="/search">To Profile and Find Study Buddy</a> -->
</body>
</html>