function toggleLogin(){
    
    var infobox = document.getElementById('info');
    
    if(infobox.hasChildNodes()){
        infobox.innerHTML = '';
    }
    /*
    var email = document.createElement('input');
    email.type = 'text';
    email.placeholder = "@pitt.edu email address";
    infobox.appendChild(email);
    
    var password = document.createElement('input');
    password.type = 'password';
    password.placeholder = "password";
    infobox.appendChild(password);
    
    var btn = document.createElement('button');
    btn.id = 'login-button';
    var text = document.createTextNode('Login');
    btn.appendChild(text);
    infobox.appendChild(btn);
    document.getElementById('login-button').onclick = function loginAccount(){
        
        alert('login acct');
    }
    
    var dont = document.createTextNode("Don't have an account?");
    infobox.appendChild(dont);
    
    var newbtn = document.createElement('button');
    newbtn.id = 'new-acct-button';
    var newtext = document.createTextNode('Create New Account');
    newbtn.appendChild(newtext);
    infobox.appendChild(newbtn);
    
    document.getElementById('new-acct-button').onclick = function newAccount(){
        
        infobox.innerHTML='';
        
        var newemail = document.createElement('input');
        newemail.type='text';
        newemail.placeholder = "@pitt.edu email address";
        infobox.appendChild(newemail);
        
        var newpw = document.createElement('input');
        newpw.type = 'password';
        newpw.placeholder = 'password';
        infobox.appendChild(newpw);
        
        var confpw = document.createElement('input');
        confpw.type = 'password';
        confpw.placeholder = 'confirm password';
        infobox.appendChild(confpw);
        
        var createacct = document.createElement('button');
        var btext = document.createTextNode('Create Account');
        createacct.appendChild(btext);
        infobox.appendChild(createacct);
        
    }
    */
    
}

function toggleAbout(){
    
    if(document.getElementById('info').hasChildNodes()){
   
        document.getElementById('info').innerHTML = '';
    }
    
    var infobox = document.getElementById('info');
    var abouttext = document.createTextNode("Stindr is the intellectual hook-up site for students of the University of Pittsburgh. Our application will assist students in finding study partners by matching them up based on their schedules and study/ personality profiles. Students will be able to pair up, organize study groups and trade textbooks. Our goal is to help students handle their schoolwork and develop such valuable teamwork and collaboration skills.");
    
    infobox.appendChild(abouttext);
    
    
}
function toggleContact(){
    
    if(document.getElementById('info')){
        document.getElementById('info').innerHTML = '';
    }
    
    var infobox = document.getElementById('info');
    var contacttext = document.createTextNode("For questions, comments, and concerns regarding Stindr please contact us at ");
    var sendmail = document.createElement('a');
    sendmail.setAttribute("href", "mailto:stindr4@gmail.com");
    var link = document.createTextNode("stindr4@gmail.com");
    sendmail.appendChild(link);
    infobox.appendChild(contacttext);
    infobox.appendChild(sendmail);
}
function toggleNewAccount(){
    
    alert('new account');
    
}